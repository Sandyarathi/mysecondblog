# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MySecondBlog::Application.config.secret_key_base = '4b21429f58fa98a231ba72a5fdff32f7caf03f6d15ccb6df027ca8c63a7a980044237a821df8e75c5316fc91ffc7c6fbdf7fcea21498bf66eedb778a3615ed1f'
